package com.example.drinks;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class ManageDrinks extends Activity {	
	
	private String toBeDeleted = "";
	
	/*
	 * Deze methode wordt opgeroepen wanneer de managedrinks activity wordt geopend
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_manage_drinks);
		
		setupActionBar();
		
		//Opnieuw hetzelfde. Registreer een handler aan de addDrink knop
		Button btnAddDrink = (Button)findViewById(R.id.btnAddDrink);
		btnAddDrink.setOnClickListener(AddDrinkHandler);
		
		//De lijst met reeds toegevoegde dranken moet opgevuld worden.
		//Op die manier kan een reeds bestaande drank verwijderd worden van het assortiment
		FillList();
		
	}

	/*
	 * Deze methode zal opgeroepen worden als je een nieuwe drank wil toevoegen
	 */
	View.OnClickListener AddDrinkHandler = new View.OnClickListener() {
		@Override
		public void onClick(View arg0) {
			
			try {
				//Voeg de nieuwe drank toe achteraan het bestand assortiment.
				//Op die manier, als je de app afsluit en weer opent, wordt het assortiment weer ingelezen
				//en moet je niet altijd alle dranken opnieuw toevoegen.
				//Het bestand assortiment zal er als volgt uitzien:
				//Pils$AB$Leffe Bl$Leffe Br$Spa$Plat water$...
				//Het karakter $ werkt als een scheidingsteken. Op die manier kun je makkelijk alle dranken weer uit 1 string
				//halen door met de methode split te werken. ( zie methode FillInList() van MainActivity.java en ReadFile van dit bestand )
				FileOutputStream fos = openFileOutput("assortiment", Context.MODE_APPEND);
				EditText txtDrinkName = (EditText)findViewById(R.id.txtDrinkName);
				String name = txtDrinkName.getText().toString() + "$";	//Voeg na de naam van de drank het scheidingsteken toe.
				fos.write(name.getBytes());
				fos.flush();
				
				//Toon een klein berichtje dat de drank opgeslagen is.
				Toast t = Toast.makeText(getApplicationContext(), "Custom drinks saved!", Toast.LENGTH_LONG);
				t.show();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//Vul de lijst opnieuw op zodanig dat je die eventueel weer kan verwijderen als je bvb. een typfout
			//gemaakt hebt.
			FillList();
		}
	};

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}
	
	//Dit is een LONG click listener. Dat wil zeggen dat je lang moet drukken op een item uit de lijst.
	//Als dit gebeurd zal deze methode opgeroepen worden om de drank uit de lijst te verwijderen.
	//Eerst zal er om een bevestiging gevraagd worden.
	OnItemLongClickListener onDrinkLongClickListener = new OnItemLongClickListener() {

		@Override
		public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
				int arg2, long arg3) {
			
			TextView txtItem = (TextView) arg1;
			ManageDrinks.this.toBeDeleted = txtItem.getText().toString();	//haal de naam van de te verwijderen drank op.
		    AlertDialog dialog = new AlertDialog.Builder(ManageDrinks.this).create();
		    dialog.setTitle("Confirmation");
		    dialog.setMessage("Do you want to delete " + txtItem.getText() + "?");
		    dialog.setCancelable(false);
		    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
		        //Deze methode hieronder zal uitgevoerd worden als de gebruiker op Yes heeft geklikt om de
		    	//geselecteerde drank te verwijderen.
		    	public void onClick(DialogInterface dialog, int buttonId) {
					String[] drinkslist = ReadFile();
					
					try {				
						FileOutputStream fos = openFileOutput("assortiment", Context.MODE_PRIVATE);
						EditText txtDrinkName = (EditText)findViewById(R.id.txtDrinkName);
						for(int i=0; i<drinkslist.length; i++) {
							//Schrijf iedere drank opnieuw weg naar het bestand assortiment, BEHALVE de drank die moet
							//verwijderd worden!
							if(!drinkslist[i].equals(ManageDrinks.this.toBeDeleted)) {
								String name = drinkslist[i] + "$";	//schrijf de naam van de drank weg + het scheidingsteken
								fos.write(name.getBytes());//Hier wordt het effectief weggeschreven
								fos.flush();
							}
						}
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//Vul de lijst opnieuw op
					FillList();

		        }
		    });
		    dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int buttonId) {
		        	//Doe niets als de gebruiker de geselecteerde drank niet wil verwijderen.
		        }
		    });
		    dialog.setIcon(android.R.drawable.ic_dialog_alert);
		    dialog.show();	//Toon het hierboven opgebouwde bevestigingsvenstertje

			return false;
		}
	};
	
	/*
	 * Deze methode zal de lijst opvullen met alle reeds toegevoegde dranken.
	 */
	private void FillList() {
		String[] customDrinksArray = ReadFile();
		if(customDrinksArray != null ) {
			ListView lstDrinks = (ListView)findViewById(R.id.lstDrinks);
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simplerow, customDrinksArray);
			
			lstDrinks.setAdapter(adapter);
			lstDrinks.setOnItemLongClickListener(onDrinkLongClickListener);
		}
	}
	
	/*
	 * Deze methode zal het bestand assortiment uitlezen en een array
	 * van alle dranknamen opvullen
	 */
	private String[] ReadFile() {

			FileInputStream fis;
			String[] customDrinksArray = null;	//Maak een lege string array aan
			try {
				fis = openFileInput("assortiment");	//Open het bestand assortiment
				DataInputStream reader = new DataInputStream(fis);
				
				byte[] file = new byte[reader.available()];
				reader.read(file, 0, file.length);
				String customDrinks = new String(file);	//in customDrinks zal het volgende zitten Pils$AB$Plat$Bruis$Leffe Bl$Leffe Br$....
				customDrinksArray = customDrinks.split("\\$");	//Splits de ingelezen tekst op het dollarteken $
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return customDrinksArray;

	}

	////////////////////Alles hieronder is automatisch gegenereerd en niet door mij geschreven!
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.manage_drinks, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}

package com.example.drinks;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.GridLayout.Spec;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

public class MainActivity extends Activity {

	private String[] assortiment = {"Cola", "Fanta"};	//Array van strings. 
	private Hashtable<String, Integer> bestelling = new Hashtable<String, Integer>();	
	//In de hashtable bestelling komt de uiteindelijke bestelling.
	//Dit is een table die er als volgt zal uitzien:
	//cola, 2
	//fanta, 4
	//pils, 3
	//AB, 1
	//...
	
	/***
	 * onCreate wordt opgeroepen wanneer de applicatie opgestart wordt.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		/* Hier registreer ik een handler op de knop ShowOverview.
		 * ( handler = een methode/functie die iets afhandelt. In dit geval,
		 * onClickListener => een handler die zal opgeroepen worden als er op de knop
		 * geklikt wordt. */
		Button btnShowOverview = (Button)findViewById(R.id.btnShowOverview);
		btnShowOverview.setOnClickListener(ShowOverviewHandler);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		/*Hier roep ik een methode op die ik zelf aangemaakt heb.*/
		FillInList();	//Zelf aangemaakte methode. Zie wat meer naar onder.
	}
	
	
	//Dit is de handler die in de onCreate methode gelinkt werd aan de knop show overview
	View.OnClickListener ShowOverviewHandler = new View.OnClickListener() {
		@Override
		public void onClick(View arg0) {
			//In het stukje hieronder wordt de hashtable bestelling volledig doorlopen
			//En wordt er een string opgebouwd dmv een stringbuilder.
			//De string ziet er dan als volgt uit:
			//Cola - 3 \nFanta - 2 \nPils - 8\n.....
			Iterator<Map.Entry<String, Integer>> it;
			Map.Entry<String, Integer> entry;
			it = bestelling.entrySet().iterator();
			StringBuilder sbuilder = new StringBuilder();
			while(it.hasNext()) {
				entry = it.next();
				sbuilder.append(entry.getValue() + " x " + entry.getKey() + "\n");
			}
			//Eenmaal de string is opgebouwd kan deze via sharedpreferences doorgegeven
			//worden naar een andere activity
			SharedPreferences settings = getSharedPreferences("DrinksFile", 0);
			SharedPreferences.Editor editor = settings.edit();
			editor.putString("Bestelling", sbuilder.toString());
			editor.commit();
			
			//Hier wordt een nieuwe activity opgestart.
			//In dit geval gaat het om de Overview activity 
			startActivity(new Intent(MainActivity.this, Overview.class));
		}
	};
	
	/***
	 * Dit is een zelf aangemaakte methode. Gewoon om de code wat overzichtelijker te houden.
	 */
	private void FillInList() {
		String[] assortimentPlusCustomDrinks = assortiment;
		try {
			//Alle zelf toegevoegde dranken worden opgeslaan in een bestand genaamd assortiment
			//Probeer hieronder het bestand uit te lezen
			FileInputStream fis = openFileInput("assortiment");
			DataInputStream reader = new DataInputStream(fis);
			
			byte[] file = new byte[reader.available()];
			reader.read(file, 0, file.length);
			String customDrinks = new String(file);
			String[] customDrinksArray = customDrinks.split("\\$");
			assortimentPlusCustomDrinks = concatStringArrays(assortiment, customDrinksArray);
		} catch (FileNotFoundException e) {
			//Als het bestand niet bestaat zal er een fout optreden. 
			//D.m.v. het try-catch block wordt de fout hier opgevangen.
			//Dan wordt er een "toast" getoond met de tekst "No custom drinks yet!"
			Toast t = Toast.makeText(getApplicationContext(), "No custom drinks yet!", Toast.LENGTH_LONG);
			t.show();
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Alle namen van dranken moeten op een knop komen waarop je uiteindelijk kunt drukken.
		//De layout is een gridlayout. 
		GridLayout drinksListUI = (GridLayout) findViewById(R.id.DrinksListUI);
		drinksListUI.removeAllViews();
		int row = 0;
		
		//D.m.v. een for-lus kun je alle beschikbare dranken overlopen.
		for(int i=0; i<assortimentPlusCustomDrinks.length; i++) {
			if(i%3 == 0 ) row++;
			
			Button btnTemp = new Button(getApplicationContext()); //Maak een tijdelijke knop aan
			btnTemp.setGravity(Gravity.CENTER_VERTICAL);
			btnTemp.setText(assortimentPlusCustomDrinks[i]);
			btnTemp.setTextSize(10);
			btnTemp.setTextColor(Color.WHITE);
			btnTemp.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
			btnTemp.setOnClickListener(addDrinksHandler);	//De handler die moet opgeroepen worden als op een drankknop gedrukt wordt
			btnTemp.setBackgroundResource(R.drawable.button_layout);	//Achtergrond van de knop
			
			GridLayout.LayoutParams lay = new GridLayout.LayoutParams(GridLayout.spec(row),GridLayout.spec(0));
			lay.width = 120;
			lay.height = 60;
			lay.bottomMargin = 15;
			lay.setGravity(Gravity.TOP|Gravity.CENTER_HORIZONTAL);
			if((i%3) == 0) lay.setGravity(Gravity.TOP|Gravity.LEFT);
			if(((i+1)%3) == 0 ) lay.setGravity(Gravity.TOP|Gravity.RIGHT);

			btnTemp.setLayoutParams(lay);
	
			drinksListUI.addView(btnTemp);	//Voeg de knop toe aan de gridlayout
		}
	}
	
	/**
	 * Deze methode wordt opgeroepen als er op een drankje geklikt is.
	 */
	View.OnClickListener addDrinksHandler = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			
			String text = (String) ((Button)v).getText();	//haal de tekst van de knop op. (bvb. Cola, Fanta, Pils, ...)
			//Kijk of er al reeds een drankje van dit type besteld is ( = in de bestelling hashtable zit )
			if(bestelling.containsKey(text)) {
				//Als er al een drankje besteld is, verhoog dan het aantal
				int value = bestelling.get(text);
				bestelling.put(text, value+1);	//aantal verhogen
			}
			else {
				bestelling.put(text, 1);	//als er nog geen drank van dit type besteld is, voeg het dan toe in de 
				//lijst bestelling
			}
			
			//Deze linearlayout bevindt zich onderaan. Hier komen alle reeds bestelde dranken in.
			LinearLayout overview = (LinearLayout)findViewById(R.id.BottomOverviewLayout);
			overview.removeAllViews();
			Iterator<Map.Entry<String, Integer>> it;
			Map.Entry<String, Integer> entry;
			it = bestelling.entrySet().iterator();
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
	                LinearLayout.LayoutParams.WRAP_CONTENT,
	                LinearLayout.LayoutParams.WRAP_CONTENT);
	        lp.setMargins(3, 3, 3, 3);

	        
			while(it.hasNext()) {
				entry = it.next();
				Button btnTemp = new Button(getApplicationContext());
				btnTemp.setText(entry.getKey() + " (" + entry.getValue() + ")");
				btnTemp.setOnClickListener(RemoveDrinksHandler);
				btnTemp.setTextSize(10);
				
				btnTemp.setBackgroundResource(R.drawable.button2_layout);
				overview.addView(btnTemp, lp);
			}
		}
	};
	
	View.OnClickListener RemoveDrinksHandler = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			
			String text = (String) ((Button)v).getText();
			text = text.substring(0, text.indexOf("(")-1);
			int value = bestelling.get(text);
			if(value == 1 ) {
				//Remove from bestelling
				bestelling.remove(text);
			}
			else{
				bestelling.put(text, value-1);
			}
			
			LinearLayout overview = (LinearLayout)findViewById(R.id.BottomOverviewLayout);
			overview.removeAllViews();
			Iterator<Map.Entry<String, Integer>> it;
			Map.Entry<String, Integer> entry;
			it = bestelling.entrySet().iterator();
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
	                LinearLayout.LayoutParams.WRAP_CONTENT,
	                LinearLayout.LayoutParams.WRAP_CONTENT);
	        lp.setMargins(3, 3, 3, 3);
	        
			while(it.hasNext()) {
				entry = it.next();
				Button btnTemp = new Button(getApplicationContext());
				btnTemp.setText(entry.getKey() + " (" + entry.getValue() + ")");
				btnTemp.setOnClickListener(RemoveDrinksHandler);
				btnTemp.setTextSize(10);
				btnTemp.setBackgroundResource(R.drawable.button2_layout);
				overview.addView(btnTemp, lp);
			}
		}
	};

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/*
	 * Als je het menuutje van de app opent, dan kun je op settings of op drinksmanager klikken.
	 * Als er op een menuitem gedrukt wordt, dan zal Android deze methode oproepen.
	 * (non-Javadoc)
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		//Hier kijk ik of het item waarop gedrukt is gelijk is aan drinksmanager
		if(item.getItemId() == R.id.DrinksManager ) {
			
			//Toast t = Toast.makeText(getApplicationContext(), "New drink selected", Toast.LENGTH_LONG);
			//t.show();
			//Als er op drinksmanager geklikt is, dan moet de drinksmanager activity geopend worden.
			startActivity(new Intent(MainActivity.this, ManageDrinks.class));
			return true;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	/*
	 * Zelf aangemaakte hulpmethode om twee arrays van strings aan elkaar te plakken.
	 * Wordt opgeroepen vanuit FillInList.
	 */
	private String[] concatStringArrays(String[] array1, String[] array2) {
		String[] result = new String[array1.length + array2.length];	//Maak een nieuwe array aan met als grootte de som van de twee arrays die aan elkaar moeten geplakt worden
		//Kopieer alle waardes van de eerste array naar de resulterende array
		for(int i=0; i<array1.length; i++) {
			result[i] = array1[i];
		}
		
		//Kopieer alle waardes van de tweede array naar de resulterende array
		for(int i=array1.length; i<result.length; i++) {
			result[i] = array2[i-array1.length];
		}
		return result; //geef het resultaat terug
	}
	
}

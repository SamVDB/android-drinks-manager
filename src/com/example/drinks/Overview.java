package com.example.drinks;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class Overview extends Activity {

	/*
	 * Deze methode wordt opgeroepen als de activity Overview geladen wordt.
	 * Hier zal de bestelling die werd doorgegeven vanaf de MainActivity via de SharedPreferences 
	 * weer opgehaald worden.
	 * (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_overview);
		TextView txtOverview = (TextView)findViewById(R.id.txtOverview);
		SharedPreferences settings = getSharedPreferences("DrinksFile", 0);
		txtOverview.setText(settings.getString("Bestelling", "Error"));
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.overview, menu);
		return true;
	}
	
	/*
	 * Deze methode wordt opgeroepen als je vanaf de overview activity het menu oproept.
	 * Je kan dan kiezen om de bestelling te delen...
	 * (non-Javadoc)
	 * @see android.app.Activity#onOptionsItemSelected(android.view.MenuItem)
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		//Test of de gebruiker zijn bestelling wil delen
		if(item.getItemId() == R.id.sendviaSMS ) {
			//Het programma komt hier terecht als de gebruiker zijn bestelling wil delen.
			
			//Toast t = Toast.makeText(getApplicationContext(), "New drink selected", Toast.LENGTH_LONG);
			//t.show();
			
			//Haal opnieuw de string op die werd doorgegeven via de shardPreferences
			SharedPreferences settings = getSharedPreferences("DrinksFile", 0);
			
			//Zorg ervoor dat Android een menu toont om te delen via SMS, Bluetooth, E-mail, WiFi-direct, .....
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT, settings.getString("Bestelling", "Error"));
			sendIntent.setType("text/plain");
			startActivity(sendIntent);
			return true;
			
		}
		
		return super.onOptionsItemSelected(item);
	}

}
